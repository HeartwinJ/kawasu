var player;
var scoreText;
var pauseScore;
var gameOverScore;
var gameOverScoreText;
var restartBtn;
var leaderboardBtn;
var obstacles = [];
var clearedObstacles = [];
var speed = 3;
var score = 0;
var playing = true;
var isGameOver = false;
var baseApiUrl = 'https://kawasu-backend.herokuapp.com';
var leaderboard = [];

var mainLayer;
var startOverlay;
var leaderboardOverlay;
var pauseOverlay;
var gameOverOverlay;

var smallFonts = window.mobileCheck();

function onMouseMove(event) {
  if (playing && !isGameOver && event.point.x > 0 && event.point.x < view.size.width) {
    player.position = new Point(event.point.x, event.point.y);
  }
}

function onKeyDown(event) {
  switch (event.key) {
    case 'r':
      reset();
      break;
    case 'escape':
      togglePause();
      break;
    case 'p':
      togglePause();
      break;
    default:
      break;
  }
}

function getLeaderboard() {
  fetch(baseApiUrl + '/api/getLeaderboard')
    .then(response => response.json())
    .then(data => {
      leaderboard = data;
      setupLeaderboardOverlay();
    });
}

function addToLeaderboard(name, score) {
  fetch(baseApiUrl + '/api/addScore', {
    method: "POST",
    headers: {
      "Content-type": "application/json; charset=UTF-8"
    },
    body: JSON.stringify({
      name: name,
      score: score
    })
  })
    .then(response => response.json())
    .then(json => { getLeaderboard() });
}

function setupLeaderboardOverlay() {
  leaderboardOverlay.activate();
  new PointText({
    point: [50, 50],
    content: 'Back',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontSize: 24,
    justification: 'left',
    opacity: 0.3
  });
  new Path.Rectangle(new Rectangle(40, 20, 80, 50), new Size(10, 10)).set({ strokeColor: '#ffffff', opacity: 0.3 });

  new PointText({
    point: [view.center.x, 150],
    content: 'Leaderboard',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontWeight: 'bold',
    fontSize: 50,
    justification: 'center'
  });

  for (var i = 0; i < leaderboard.length; i++) {
    const pos = leaderboard[i];
    new PointText({
      point: [50, 200 + ((i + 1) * 50)],
      content: (i+1).toString(),
      fillColor: '#ffffff',
      fontFamily: 'Courier New',
      fontSize: 24,
      justification: 'center'
    });
    new PointText({
      point: [85, 200 + ((i + 1) * 50)],
      content: pos.name.replaceAll('\"', ''),
      fillColor: '#ffffff',
      fontFamily: 'Courier New',
      fontSize: 24,
      justification: 'left'
    });
    new PointText({
      point: [view.size.width - 50, 200 + ((i + 1) * 50)],
      content: pos.score,
      fillColor: '#ffffff',
      fontFamily: 'Courier New',
      fontSize: 24,
      justification: 'right'
    });
  }
  mainLayer.activate();
}

function showLeaderboard() {
  leaderboardOverlay.insertAbove(startOverlay);
}

function reset() {
  for (var i = 0; i < obstacles.length; i++) {
    obstacles[i].remove();
  }
  player.position = [view.size.width / 2, view.size.height - 70];
  obstacles = [];
  clearedObstacles = [];
  speed = 3;
  score = 0;
  isGameOver = false;
  playing = true;
  gameOverScoreText.content = 'Your Lame Score';

  init();
  startOverlay.remove();
  pauseOverlay.remove();
  gameOverOverlay.remove();
  view.play();
}

function gameOver() {
  isGameOver = true;
  view.pause();
  togglePause();
  gameOverScore.content = score;
  if (score > leaderboard[leaderboard.length - 1].score) {
    var name = prompt('Hooray! You made it to the Leaderboard!\nEnter Your Name');
    name = name == '' ? 'AwesomeChicken': name;
    gameOverScoreText.content = 'Dayuuum! You Scored';
    addToLeaderboard(name, score);
  }
  gameOverOverlay.insertAbove(mainLayer);

}

function togglePause() {
  playing ? pauseGame() : resumeGame();
}

function pauseGame() {
  view.pause();
  playing = false;
  if (!isGameOver) {
    pauseScore.content = score;
    pauseOverlay.insertAbove(mainLayer);
  }
}

function resumeGame() {
  if (!isGameOver) {
    playing = true;
    pauseOverlay.remove();
    view.play();
  }
}

function addScore(id) {
  if (!clearedObstacles.includes(id)) {
    clearedObstacles.push(id);
    score++;
  }
}

function genObstacle(xOff, yOff) {
  obstacles.push(new Group({
    children: [
      new Path.Rectangle({
        point: [xOff - 175, yOff],
        size: [25, 25],
        fillColor: '#f3c086'
      }),
      new Path.Rectangle({
        point: [xOff, yOff],
        size: [view.size.width - 150, 25],
        fillColor: '#f3c086'
      }),
      new Path.Rectangle({
        point: [view.size.width + xOff, yOff],
        size: [view.size.width - 150, 25],
        fillColor: '#f3c086'
      })
    ],
    onFrame: function () {
      this.translate(new Point(0, speed))
      if (view.size.height < this.position.y) {
        genObstacle((Math.random() < 0.5 ? 150 : 0) * (Math.random() < 0.5 ? -1 : 1), obstacles[obstacles.length - 1].getItem().position.y - 150);
        this.remove();
        var curObj = this;
        obstacles = obstacles.filter(function (obst) { return obst != curObj })
      }

      if (this.intersects(player)) {
        if (this.children[0].intersects(player) || this.children[1].intersects(player) || this.children[2].intersects(player)) {
          this.children[0].fillColor = this.children[1].fillColor = this.children[2].fillColor = '#c97e81';
          gameOver();
        } else {
          addScore(this.id);
        }
      }
    }
  }));
}

function drawBackground() {
  var bgRect = new Rectangle([0, 0], [view.size.width, view.size.height]);
  var bgRectPath = new Path.Rectangle(bgRect);
  bgRectPath.sendToBack();
  bgRectPath.fillColor = '#ffffff';
}

function showStartOverlay() {
  view.pause();
  startOverlay.insertAbove(mainLayer);
}

function setupScreenComponents() {
  scoreText = new PointText(new Point(view.center.x, 100));
  scoreText.fillColor = '#5a7b7b';
  scoreText.fontSize = 48;
  scoreText.fontFamily = 'Courier New'
  scoreText.justification = 'center';

  restartBtn = new Rectangle([view.center.x - 125, view.size.height - 250], [250, 100]);

  startOverlay.activate();
  new PointText({
    point: [view.center.x, 100],
    content: 'Suppp?',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontWeight: 'bold',
    fontSize: 50,
    justification: 'center'
  });
  new PointText({
    point: [view.center.x, view.center.y - 80],
    content: 'The game is pretty basic.',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontSize: smallFonts ? 18 : 28,
    justification: 'center'
  });
  new PointText({
    point: [view.center.x, view.center.y - 50],
    content: 'Move around using your mouse.',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontSize: smallFonts ? 16 : 24,
    justification: 'center'
  });
  new PointText({
    point: [view.center.x, view.center.y - 20],
    content: 'Do not hit Obstacles.',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontSize: smallFonts ? 16 : 24,
    justification: 'center'
  });
  new PointText({
    point: [view.center.x, view.center.y + 40],
    content: 'Have Fun!',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontSize: 50,
    justification: 'center'
  });
  new PointText({
    point: [view.center.x, view.size.height - 185],
    content: 'START',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontSize: 48,
    justification: 'center',
    opacity: 0.3
  });
  leaderboardBtn = new Path.Circle({
    center: [view.center.x, view.size.height - 75],
    radius: 50,
    strokeColor: '#ffffff',
    opacity: 0.3
  });
  new Group({
    children: [
      new Path.Rectangle(new Point(view.center.x - 30, view.size.height - 70), new Size(15, 20)).set({ fillColor: '#ffffff', opacity: 0.3 }),
      new Path.Rectangle(new Point(view.center.x - 10, view.size.height - 100), new Size(15, 50)).set({ fillColor: '#ffffff', opacity: 0.3 }),
      new Path.Rectangle(new Point(view.center.x + 10, view.size.height - 80), new Size(15, 30)).set({ fillColor: '#ffffff', opacity: 0.3 })
    ],
    position: [view.center.x, view.size.height - 75]
  });
  new Path.Rectangle(restartBtn, new Size(10, 10)).set({ strokeColor: '#ffffff', opacity: 0.3 });

  pauseOverlay.activate();
  new PointText({
    point: [view.center.x, 100],
    content: 'Paused',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontWeight: 'bold',
    fontSize: 50,
    justification: 'center'
  });
  new PointText({
    point: [view.center.x, 150],
    content: 'You had to leave me! T.T',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontSize: 24,
    justification: 'center'
  });
  new PointText({
    point: [view.center.x, view.center.y - 25],
    content: 'Your Lame Score',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontSize: 36,
    justification: 'center'
  });
  pauseScore = new PointText({
    point: [view.center.x, view.center.y + 25],
    content: score,
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontWeight: 'bold',
    fontSize: 50,
    justification: 'center'
  });
  new PointText({
    point: [view.center.x, view.size.height - 100],
    content: 'Press \'Esc\' or \'P\' to Resume',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontWeight: 'bold',
    fontSize: 25,
    justification: 'center'
  });

  gameOverOverlay.activate();
  new PointText({
    point: [view.center.x, 100],
    content: 'Hehe,You Lost!',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontWeight: 'bold',
    fontSize: smallFonts ? 40 : 48,
    justification: 'center'
  });
  gameOverScoreText = new PointText({
    point: [view.center.x, view.center.y - 25],
    content: 'Your Lame Score',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontSize: smallFonts ? 28 : 36,
    justification: 'center'
  });
  gameOverScore = new PointText({
    point: [view.center.x, view.center.y + 25],
    content: score,
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontWeight: 'bold',
    fontSize: 50,
    justification: 'center'
  });
  new PointText({
    point: [view.center.x, view.size.height - 185],
    content: 'RESTART',
    fillColor: '#ffffff',
    fontFamily: 'Courier New',
    fontSize: 48,
    justification: 'center',
    opacity: 0.3
  });
  new Path.Rectangle(restartBtn, new Size(10, 10)).set({ strokeColor: '#ffffff', opacity: 0.3 });

  mainLayer.activate();

  drawScreenComponents();
}

function drawScreenComponents() {
  scoreText.bringToFront();
  scoreText.content = score;
}

function init() {
  for (var i = 0; i < 10; i++) {
    genObstacle(
      (Math.random() < 0.5 ? 150 : 0) * (Math.random() < 0.5 ? -1 : 1),
      i == 0 ? 0 : obstacles[i - 1].getItem().position.y - 150
    )
  }
}

function setup() {
  getLeaderboard();
  player = Path.Rectangle({
    point: [view.size.width / 2, view.size.height - 70],
    size: [25, 25],
    fillColor: '#74aeb5'
  });

  mainLayer = project.activeLayer;
  startOverlay = new Layer();
  new Path.Rectangle({
    position: view.center,
    size: [view.size.width, view.size.height],
    fillColor: '#232531'
  });
  startOverlay.onClick = function (event) {
    if (event.point.isInside(restartBtn)) {
      reset();
    } else if (event.point.isInside(leaderboardBtn.bounds)) {
      showLeaderboard();
    }
  }
  startOverlay.remove();
  leaderboardOverlay = new Layer();
  leaderboardOverlay.onClick = function (event) {
    if (event.point.isInside(new Rectangle(40, 20, 80, 50))) {
      leaderboardOverlay.remove();
    }
  }
  new Path.Rectangle({
    position: view.center,
    size: [view.size.width, view.size.height],
    fillColor: '#232531'
  });
  leaderboardOverlay.remove();
  pauseOverlay = new Layer();
  new Path.Rectangle({
    position: view.center,
    size: [view.size.width, view.size.height],
    fillColor: '#232531'
  });
  pauseOverlay.remove();
  gameOverOverlay = new Layer();
  new Path.Rectangle({
    position: view.center,
    size: [view.size.width, view.size.height],
    fillColor: '#232531'
  });
  gameOverOverlay.onClick = function (event) {
    if (event.point.isInside(restartBtn)) {
      reset();
    }
  }
  gameOverOverlay.remove();
  mainLayer.activate();

  drawBackground();
  setupScreenComponents();
  showStartOverlay();
}

function onFrame() {
  speed += 0.001;
  drawScreenComponents();
}

setup();